#!/usr/bin/python3

import argparse
import collections
import json
import logging
import os
import subprocess


class ActionEnv(argparse.Action):
    def __init__(self, env, default=None, required=True, **kw):
        if env:
            default = os.environ.get(env, default)
        if default:
            required = False
        super().__init__(default=default, required=required, **kw)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values)


class CloudRelease(collections.OrderedDict):
    env = [
        ('id', 'CLOUD_RELEASE_ID'),
        ('version', 'CLOUD_RELEASE_VERSION'),
        ('build_info_url', 'CI_JOB_URL'),
    ]

    def __init__(self):
        super().__init__()
        for name, env in self.env:
            v = os.environ.get(env)
            if v:
                self[name] = v

    def write(self, f):
        for k, v in self.items():
            print('{}="{}"'.format(k.upper(), v), file=f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--basename',
        help='(default: $CLOUD_BUILD_BASENAME)',
        action=ActionEnv,
        env='CLOUD_BUILD_BASENAME',
    )
    parser.add_argument('--root',
        help='(default: $FAI_ROOT)',
        action=ActionEnv,
        env='FAI_ROOT',
    )
    parser.add_argument('--write',
        action=ActionEnv,
        required=False,
        env='FAI_ACTION',
    )

    args = parser.parse_args()

    cloud_release = CloudRelease()
    manifest = {
        'cloud_release': cloud_release,
        'package_versions': {},
    }

    with subprocess.Popen(
        ('dpkg-query', '-W', '--admindir', os.path.join(args.root, 'var/lib/dpkg')),
        stdout=subprocess.PIPE,
    ) as f:
        for l in f.stdout:
            package, version = l.decode().strip().split()
            manifest['package_versions'][package] = version

    with open('{}.build.json'.format(args.basename), 'w') as f:
        json.dump(manifest, f, indent=4, separators=(',', ': '), sort_keys=True)

    if args.write:
        with open(os.path.join(args.root, 'etc/cloud-release'), 'w') as f:
            cloud_release.write(f)
